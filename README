cpu-avgfreq - measure average cpu frequency with aperf/mperf counters

Author: Tomas Glozar <tglozar@redhat.com>

Usage: cpu-avgfreq.pl [-m] <cpus> <interval>
- cpus: comma-separated list
- interval: interval between measurements in seconds
- -m: measure mperf difference instead of average frequency

cpu-avgfreq periodically measures the average frequency of the core in a given
time period or the mperf difference when the -m option is given. The latter is
useful to trace idle states since mperf is only incremented in C0.

No cpufreq driver is needed since it directly reads Intel-specific aperf and
mperf MSRs. That also means the tool works only on Intel CPUs that are new
enough to support these.

The accuracy of the measurements of the tool depends on the delay between
the reading of the two counters: with smaller delay comes higher accuracy.
Therefore, it is recommended to run the tool with high priority.

Best results were obtained by measuring on an isolated thread-sibling of
the desired core on which the applications is running.

For example, for an application on cpu 4 with cpu 84 being its isolated sibling:

$ taskset --cpu-list 84 chrt 10 ./cpufreq.pl 84 0.01

Other than priority/isolation, the accuracy also varies depending on the overall
time spent in idle states, since the counters only increase in C0.
