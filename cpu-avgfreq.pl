#!/usr/bin/env perl
use v5.010;
use strict;
use warnings;
use Fcntl qw(SEEK_CUR);

use constant {
	MSR_IA32_MPERF => 0x000000e7,
	MSR_IA32_APERF => 0x000000e8
};

sub read_msr {
	my ($cpu, $msr) = @_;
	open my $fh, '<', "/dev/cpu/$cpu/msr" or die;
	binmode $fh;
	my $buf = '';
	seek $fh, $msr, SEEK_CUR or die;
	read $fh, $buf, 8 or die;
	return unpack 'Q', $buf;
}

if (@ARGV != 2 && @ARGV != 3) {
	say "Usage: cpufreq.pl [-m] <cpus> <interval>";
	exit 0;
}
my $mperf_only = 0;
if ($ARGV[0] eq "-m") {
	$mperf_only = 1;
	shift @ARGV;
}
my @cpus = split(',', $ARGV[0]);
my $interval = $ARGV[1];

my %mperf, my %aperf;
STDOUT->autoflush(1);
while (1) {
	for my $cpu (@cpus) {
		my $aperf = read_msr($cpu, MSR_IA32_APERF);
		my $mperf = read_msr($cpu, MSR_IA32_MPERF);

		if (exists($mperf{$cpu})) {
			my $mperf_diff = $mperf - $mperf{$cpu};
			my $aperf_diff = $aperf - $aperf{$cpu};
			if ($mperf_only) {
				say $mperf_diff;
			} else {
				say ($aperf_diff / $mperf_diff);
			}
		}

		$mperf{$cpu} = $mperf;
		$aperf{$cpu} = $aperf;
	}
	select(undef, undef, undef, $interval);
}
